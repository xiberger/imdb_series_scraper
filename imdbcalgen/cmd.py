from clint.textui import puts, prompt, colored

from imdbcalgen.calgen import create_ical, fetch_seasons


class Generator:
    def __init__(self, imdb_id=None):
        self.imdb_id = imdb_id

    def run(self):
        if self.imdb_id is None:
            self.imdb_id = prompt.query("Insert imdb id")

        seasons = fetch_seasons(self.imdb_id)
        if seasons is None or len(seasons) == 0:
            puts(colored.red("no seasons available"))
            return
        else:
            if len(seasons) > 1:
                puts("Available Seasons " + str(seasons[0]) + "-" + str(seasons[len(seasons) - 1]))
            elif len(seasons) == 1:
                puts("Available Seasons 1")

        while True:
            omit_promt = prompt.query("omit already aired seasons [Y/n]")
            if str(omit_promt[0]).lower() == 'y':
                omit = True
                break
            elif str(omit_promt[0]).lower() == 'n':
                omit = False
                break

        create_ical(self.imdb_id, omit_aired=omit)
        puts(colored.green("created ical file"))