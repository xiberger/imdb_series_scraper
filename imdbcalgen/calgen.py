from datetime import datetime, timedelta
from sys import stderr
from typing import List, Dict, Optional
from uuid import uuid1

from icalendar import Calendar, Event
from imdbpie import Imdb


def _create_start_date(date: str) -> Optional[datetime]:
    if date is None:
        return None
    split = date.split('-')
    if len(split) != 3:
        return None
    return datetime(int(split[0]), int(split[1]), int(split[2]))


## todo check for wrong elements

def _create_events(episodes: List[Dict], series_title: str, season: int, omit_aired=False) -> List[Event]:
    events = []
    for episode in episodes:
        start_date = _create_start_date(episode['releaseDate']['first']['date'])
        episode_id = "S" + str(season).zfill(2) + "E" + str(episode["episodeNumber"]).zfill(2)

        if start_date is None:
            print("no valid date for " + episode_id, file=stderr)
            continue

        end_date = start_date + timedelta(days=1)
        now = datetime.now()

        if omit_aired and start_date < now:
            continue

        event = Event()
        event.add("uid", str(uuid1()))
        event.add("summary", series_title + " " + episode['title'] + " " + episode_id)

        event["dtstart"] = start_date.strftime("%Y%m%d")
        event["dtend"] = end_date.strftime("%Y%m%d")
        event.add("dtstamp", datetime.now())

        event.add("X-FUNAMBOL-ALLDAY", 1)
        event.add("X-MICROSOFT-CDO-ALLDAYEVENT", 1)
        events.append(event)
    return events


def fetch_seasons(imdb_id):
    imdb = Imdb()
    title_episodes = imdb.get_title_episodes(imdb_id)
    return [k + 1 for k in range(len(title_episodes['seasons']))]


## create opts title which is id, check if is series
def create_ical(imdb_id, file_name=None, seasons=None, omit_aired=False):
    imdb = Imdb()

    if seasons is None or len(seasons) == 0:
        seasons = fetch_seasons(imdb_id)

    cal = Calendar()
    cal.add('version', '2.0')
    title = imdb.get_title(imdb_id)
    name = title['base']['title']
    for season_no in seasons:
        season_info = imdb.get_title_episodes_detailed(imdb_id, season=season_no)
        episodes = season_info['episodes']
        for event in _create_events(episodes, name, season_no, omit_aired):
            cal.add_component(event)

    cal_id = "".join(name.split()) + str(uuid1())
    cal.add('prodid', cal_id)
    cal.add("uid", cal_id)

    if file_name is None:
        file_name = name + ".ics"

    with open(file_name, "wb") as fp:
        fp.write(cal.to_ical())
