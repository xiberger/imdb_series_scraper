from argparse import ArgumentParser
from imdbcalgen import create_ical
# include omdb - http://www.omdbapi.com/
# todo add search functions separate file
if __name__ == '__main__':
    parser = ArgumentParser("Creates an ics file for a season specified by the IMDB id")
    parser.add_argument("imdbid", help="Imbd title", type=str)
    parser.add_argument("seasons", metavar="S", type=int, nargs="*", help="Seasons to include")
    parser.add_argument("-oa", action="store_true", help="omit already aired episodes")
    parser.add_argument("-o", type=str, help="output file")
    args = parser.parse_args()
    create_ical(args.imdbid, seasons=args.seasons, omit_aired=args.oa)
