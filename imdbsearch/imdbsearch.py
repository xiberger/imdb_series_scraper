from clint.textui import puts, prompt, indent, colored, validators
from imdbpie import Imdb

from imdbcalgen import Generator


class Search:
    def __init__(self):
        self.running = True
        self.imdb = Imdb()

    def run(self):

        while self.running:
            query = prompt.query("What Series are you looking for?")
            series = self.query(query)
            if len(series) > 0:
                needs_selection = True
                while needs_selection:
                    for i, s in enumerate(series):
                        with indent(2, quote=">"):
                            puts(colored.blue(str(i + 1)) + " " + s['title'] + " (" + s['year'] + ")")
                    puts(colored.red("0 none and EXIT"))
                    selection = prompt.query("Your choice: ", validators=[validators.IntegerValidator()])

                    if selection == 0:
                        needs_selection = False
                        self.running = False
                    elif 0 < selection < len(series):
                        selected_series = series[selection - 1]
                        puts("creating ical file for " + selected_series['title'])
                        Generator(selected_series['imdb_id']).run()
                        self.running = False
                        needs_selection = False
                    else:
                        puts(colored.red("not a valid selection"))

    def query(self, query):
        results = self.imdb.search_for_title(query)
        return [result for result in results if result is not None and "series" in str(result['type']).lower()]
